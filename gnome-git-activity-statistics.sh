#!/bin/bash
# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode
echo "Run this script in the parent directory which has all up to date Git repositories as subfolders."
echo "Before running this script, make sure that the DEADLINE variable is up to date."
echo "You can cancel running this script via Ctrl+C."
sleep 10

# The tuesday of the last .0 release, between tarballs due and release day.
# For example, if running this script for the 3.38.0 releease, DEADLINE
# should be set to the Tuesday before the previous 3.36.0 release.
DEADLINE="2019-03-12"

# TODO: Check
# git log --raw --numstat --pretty=fuller --decorate=full --parents --reverse --topo-order -M -C -c --remotes=origin --all | grep ^AuthorDate | wc -l
# which might cover all branches?

# TODO: Add explicit check to make sure we're on either main or master branch (but default anyway)?

cd GNOME/

TOTAL_COMMITS=0
for i in $( ls ); do
        if [ -d $i ]; then
                cd $i
                if [ -d ".git" ]; then # check whether dir is git checkout = skip extracted tarballs, e.g. when in jhbuild
                        # Total number of commits:
                        COMMITS=$(git log --after=$DEADLINE --pretty=oneline | wc -l)
                        TOTAL_COMMITS=$((TOTAL_COMMITS + COMMITS))
                fi
        cd .. # leave module
        fi
done
echo "Total number of commits per module, only in git master: ${TOTAL_COMMITS}"

declare -A AUTHORS
for i in $( ls ); do
        if [ -d $i ]; then
                cd $i
#		echo "======" $i
                if [ -d ".git" ]; then # check whether dir is git checkout = skip extracted tarballs, e.g. when in jhbuild
                        # Get all authors for all the commits, sorted by number of commits:
                        for AUTHOR in $(git log --after=$DEADLINE --author='' --pretty=format:"%ae" | sort | uniq); do
                                AUTHORS[$AUTHOR]=1
                        done
                fi
        cd .. # leave module
        fi
done
echo "Unique contributors: ${#AUTHORS[@]}"

cd ..
